package cl.app.dualsimdatawidget;

/**
 * Created by focmb on 01.10.13.
 */

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;

public class HelloWidget extends AppWidgetProvider
{
    Context context = null;

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
    {
        RemoteViews rViews = new RemoteViews(context.getPackageName(), R.layout.main);

        TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String subscriberId = tm.getSubscriberId();

        rViews.setTextViewText(R.id.widget_textview, String.valueOf(subscriberId));

        pushWidgetUpdate(context, rViews);
    }

    public static void pushWidgetUpdate(Context context,RemoteViews views)
    {

        ComponentName myWidget=new ComponentName(context, HelloWidget.class);
        AppWidgetManager manager=AppWidgetManager.getInstance(context);
        manager.updateAppWidget(myWidget, views);
    }

}
