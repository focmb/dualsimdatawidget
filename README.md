# Info
Android Widget zum gleichzeitigen Anzeigen des mobilen Datenverbrauchs zweier Sim-Karten auf einem Dual-Sim-Phone (z. B. Jiayu G3S).

# TODO
* Sim-Karten über Telephony-Manager ansprechen
* Widget bei Änderung automatisch updaten
* Fortschrittsbalken einbauen
* Logo erstellen
* beim Start Abfrage, welche Sim-Karte für mobile Daten geschalten ist - Listener für einen Wechsel der Simkarte (z. B. für welche Karte ist TelephonyManager DATA_CONNECTED = 2)
* Eingabe erstellen, damit Startgröße eingegeben werden kann, wieviel Traffic bereits verbraucht wurde (ist wichtig, da das Widget nicht unbedingt zum Monatsanfang oder so gestartet werden muss)
* Variable (2 Stück, da 2 Simkarten) zum speichern des Traffics einbauen, da die Datei des Systems, aus der Wert ausgelesen wird, bei jedem Reboot neu erzeugt wird und die Zählung wieder bei 0 anfängt.

***PS:*** Da es sich nur um ein kleineres Nebenprojekt handelt, werde ich nur hin und wieder ein paar Zeilen Code basteln, so dass sich die Fertigstellung wohl etwas hinziehen wird.
